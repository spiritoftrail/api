package com.spiritoftrail.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Encrypt {
    private Encrypt() {
        throw new IllegalStateException("Encrypt class");
    }

    public static String hash(String sha, String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance(sha);
            byte[] hash = digest.digest(s.getBytes(StandardCharsets.UTF_8));

            StringBuilder hexString = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1)
                    hexString.append('0');

                hexString.append(hex);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException ignored) {
            return null;
        }
    }
}

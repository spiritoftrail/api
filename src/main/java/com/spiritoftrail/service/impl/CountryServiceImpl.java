package com.spiritoftrail.service.impl;

import com.spiritoftrail.dto.*;
import com.spiritoftrail.model.Country;
import com.spiritoftrail.repository.CountryRepository;
import com.spiritoftrail.service.CountryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<CountryCodeDto> code() {
        List<CountryCodeDto> code = new ArrayList<>();

        List<Country> countries = countryRepository.findAllByOrderByName();

        for(Country country : countries)
            code.add(new CountryCodeDto(country.getId(), country.getName()));

        return code;
    }
}

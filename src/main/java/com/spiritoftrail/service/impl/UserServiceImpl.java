package com.spiritoftrail.service.impl;

import com.spiritoftrail.common.Constants;
import com.spiritoftrail.dto.*;
import com.spiritoftrail.model.Country;
import com.spiritoftrail.model.Gender;
import com.spiritoftrail.model.Level;
import com.spiritoftrail.model.User;
import com.spiritoftrail.repository.CountryRepository;
import com.spiritoftrail.repository.GenderRepository;
import com.spiritoftrail.repository.LevelRepository;
import com.spiritoftrail.repository.UserRepository;
import com.spiritoftrail.service.UserService;
import com.spiritoftrail.util.Encrypt;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final GenderRepository genderRepository;
    private final CountryRepository countryRepository;
    private final LevelRepository levelRepository;

    public UserServiceImpl(UserRepository userRepository, GenderRepository genderRepository, CountryRepository countryRepository, LevelRepository levelRepository) {
        this.userRepository = userRepository;
        this.genderRepository = genderRepository;
        this.countryRepository = countryRepository;
        this.levelRepository = levelRepository;
    }

    @Override
    public SignupUserStatusDto signupUser(SignupUserDto user) {
        List<SignupUserErrorDto> error = new ArrayList<>();

        Country country = null;
        Gender gender = null;

        int birthday = 0;
        int year = Calendar.getInstance().get(Calendar.YEAR);

        if(user.firstname() == null)
            error.add(new SignupUserErrorDto(Constants.USER_FIRSTNAME, Constants.ERROR_USER_FIRSTNAME_UNSPECIFIED));
        else if(user.firstname().trim().length() > Constants.MODEL_USER_FIRSTNAME_MAXLENGTH)
            error.add(new SignupUserErrorDto(Constants.USER_FIRSTNAME, String.format(Constants.ERROR_USER_FIRSTNAME_MAXLENGTH, Constants.MODEL_USER_FIRSTNAME_MAXLENGTH)));

        if(user.lastname() == null)
            error.add(new SignupUserErrorDto(Constants.USER_LASTNAME, Constants.ERROR_USER_LASTNAME_UNSPECIFIED));
        else if(user.lastname().trim().length() > Constants.MODEL_USER_LASTNAME_MAXLENGTH)
            error.add(new SignupUserErrorDto(Constants.USER_LASTNAME, String.format(Constants.ERROR_USER_LASTNAME_MAXLENGTH, Constants.MODEL_USER_LASTNAME_MAXLENGTH)));

        if(user.email() == null)
            error.add(new SignupUserErrorDto(Constants.USER_EMAIL, Constants.ERROR_USER_EMAIL_UNSPECIFIED));
        else if(user.email().trim().length() > Constants.MODEL_USER_EMAIL_MAXLENGTH)
            error.add(new SignupUserErrorDto(Constants.USER_EMAIL, String.format(Constants.ERROR_USER_EMAIL_MAXLENGTH, Constants.MODEL_USER_EMAIL_MAXLENGTH)));

        if(user.password() == null)
            error.add(new SignupUserErrorDto(Constants.USER_PASSWORD, Constants.ERROR_USER_PASSWORD_UNSPECIFIED));
        else if(user.password().length() < Constants.MODEL_USER_PASSWORD_MINLENGTH)
            error.add(new SignupUserErrorDto(Constants.USER_PASSWORD, String.format(Constants.ERROR_USER_PASSWORD_MINLENGTH, Constants.MODEL_USER_PASSWORD_MINLENGTH)));
        else if(user.password().length() > Constants.MODEL_USER_PASSWORD_MAXLENGTH)
            error.add(new SignupUserErrorDto(Constants.USER_PASSWORD, String.format(Constants.ERROR_USER_PASSWORD_MAXLENGTH, Constants.MODEL_USER_PASSWORD_MAXLENGTH)));
        else if(!user.password().equals(user.repassword()))
            error.add(new SignupUserErrorDto(Constants.USER_PASSWORD, Constants.ERROR_USER_PASSWORD_DIFFERENT));

        if(user.birthday() == null)
            error.add(new SignupUserErrorDto(Constants.USER_BIRTHDAY, Constants.ERROR_USER_BIRTHDAY_UNSPECIFIED));
        else {
            try {
                birthday = Integer.parseInt(user.birthday());

                if(birthday < year - 120 || birthday > year)
                    error.add(new SignupUserErrorDto(Constants.USER_BIRTHDAY, Constants.ERROR_USER_BIRTHDAY_INVALID));

            } catch (NumberFormatException ignored) {
                error.add(new SignupUserErrorDto(Constants.USER_BIRTHDAY, Constants.ERROR_USER_BIRTHDAY_INVALID));
            }
        }

        if(user.gender() == null)
            error.add(new SignupUserErrorDto(Constants.USER_GENDER, Constants.ERROR_USER_GENDER_UNSPECIFIED));
        else {
            gender = genderRepository.findById(user.gender().trim());

            if(gender == null)
                error.add(new SignupUserErrorDto(Constants.USER_GENDER, Constants.ERROR_USER_GENDER_INVALID));
        }

        if(user.country() == null)
            error.add(new SignupUserErrorDto(Constants.USER_COUNTRY, Constants.ERROR_USER_COUNTRY_UNSPECIFIED));
        else {
            country = countryRepository.findById(user.country().trim());

            if(country == null)
                error.add(new SignupUserErrorDto(Constants.USER_COUNTRY, Constants.ERROR_USER_COUNTRY_INVALID));
        }

        if(error.isEmpty()) {
            User newUser = userRepository.findUserByEmail(user.email().trim());

            if(newUser != null && newUser.getEmail() != null) {
                error.add(new SignupUserErrorDto(Constants.USER_EMAIL, Constants.ERROR_USER_EMAIL_EXIST));
            } else {
                Level level = levelRepository.findByName("Visiteur");
                userRepository.save(new User(user.firstname().trim(), user.lastname().trim(), user.email().trim(), Encrypt.hash("SHA-256", user.password()), birthday, country, gender, new Date(), level, null, false, false));

                return new SignupUserStatusDto(true, null);
            }
        }

        return new SignupUserStatusDto(false, error);
    }

    @Override
    public SigninUserDto signinUser(String email, String password) {
        User user = userRepository.findByEmailAndPassword(email, Encrypt.hash("SHA-256", password));

        if(user == null)
            user = userRepository.findByEmailAndPassword(email, Encrypt.hash("SHA-1", password));

        return (user != null) ? new SigninUserDto(true, user.getId(), user.getFirstname(), user.getLastname(), user.getLevel().getId(), null) : null;
    }

    @Override
    public List<ItraUserDto> allItraUser() {
        List<ItraUserDto> itraUser = new ArrayList<>();

        List<User> users = userRepository.findAll();
        for(User user : users)
            itraUser.add(new ItraUserDto(user.getId(), user.getItra()));

        return itraUser;
    }
}

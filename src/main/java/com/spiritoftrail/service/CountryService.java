package com.spiritoftrail.service;

import com.spiritoftrail.dto.CountryCodeDto;

import java.util.List;

public interface CountryService {
    List<CountryCodeDto> code();
}

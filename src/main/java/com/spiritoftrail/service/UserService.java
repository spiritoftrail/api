package com.spiritoftrail.service;

import com.spiritoftrail.dto.ItraUserDto;
import com.spiritoftrail.dto.SigninUserDto;
import com.spiritoftrail.dto.SignupUserDto;
import com.spiritoftrail.dto.SignupUserStatusDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {
    SignupUserStatusDto signupUser(SignupUserDto user);
    SigninUserDto signinUser(String email, String password);
    List<ItraUserDto> allItraUser();
}

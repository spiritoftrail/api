package com.spiritoftrail.common;

public class Constants {

    private Constants() {
        throw new IllegalStateException("Constants class");
    }

    public static final int MODEL_USER_FIRSTNAME_MAXLENGTH = 32;
    public static final int MODEL_USER_LASTNAME_MAXLENGTH = 32;
    public static final int MODEL_USER_EMAIL_MAXLENGTH = 128;
    public static final int MODEL_USER_PASSWORD_MINLENGTH = 6;
    public static final int MODEL_USER_PASSWORD_MAXLENGTH = 128;

    public static final String USER_FIRSTNAME = "firstname";
    public static final String USER_LASTNAME = "lastname";
    public static final String USER_EMAIL = "email";
    public static final String USER_PASSWORD = "password";
    public static final String USER_BIRTHDAY = "birthday";
    public static final String USER_GENDER = "gender";
    public static final String USER_COUNTRY = "country";

    public static final String ERROR_USER_FIRSTNAME_UNSPECIFIED = "Le prénom doit être renseigné";
    public static final String ERROR_USER_FIRSTNAME_MAXLENGTH = "Le prénom doit comporter au maximum %d caractères";
    public static final String ERROR_USER_LASTNAME_UNSPECIFIED = "Le nom doit être renseigné";
    public static final String ERROR_USER_LASTNAME_MAXLENGTH = "Le nom doit comporter au maximum %d caractères";
    public static final String ERROR_USER_EMAIL_UNSPECIFIED = "L'adresse mail doit être renseignée";
    public static final String ERROR_USER_EMAIL_MAXLENGTH = "L'adresse mail doit comporter au maximum %d caractères";
    public static final String ERROR_USER_EMAIL_EXIST = "Cette adresse email est déjà utilisée pour un autre compte";
    public static final String ERROR_USER_PASSWORD_UNSPECIFIED = "Le mot de passe doit être renseigné";
    public static final String ERROR_USER_PASSWORD_MINLENGTH = "Le mot de passe doit comporter au minimum %d caractères";
    public static final String ERROR_USER_PASSWORD_MAXLENGTH = "Le mot de passe doit comporter au maximum %d caractères";
    public static final String ERROR_USER_PASSWORD_DIFFERENT = "Les mots de passes saisis ne sont pas identiques";
    public static final String ERROR_USER_BIRTHDAY_UNSPECIFIED = "La date de naissance doit être renseignée";
    public static final String ERROR_USER_BIRTHDAY_INVALID = "La date de naissance n'est pas une date valide";
    public static final String ERROR_USER_GENDER_UNSPECIFIED = "Le genre doit être renseigné";
    public static final String ERROR_USER_GENDER_INVALID = "Le genre n'est pas une saisie valide";
    public static final String ERROR_USER_COUNTRY_UNSPECIFIED = "Le pays doit être renseigné";
    public static final String ERROR_USER_COUNTRY_INVALID = "Le pays n'est pas une saisie valide";
}

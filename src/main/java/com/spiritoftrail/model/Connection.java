package com.spiritoftrail.model;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "connection")
public class Connection implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "id", nullable = false, updatable = false)
    private User user;

    @Column(name = "last")
    private Date last;

    @Column(name = "number")
    private Integer number;

    public Connection() {}

    public Connection(User user, Date last, Integer number) {
        this.user = user;
        this.last = last;
        this.number = number;
    }

    public User getUser() {
        return user;
    }

    public Date getLast() {
        return last;
    }

    public Integer getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Connection that = (Connection) o;
        return Objects.equals(user, that.user) && Objects.equals(last, that.last) && Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, last, number);
    }
}

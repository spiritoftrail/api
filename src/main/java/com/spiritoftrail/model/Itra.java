package com.spiritoftrail.model;

import jakarta.persistence.*;

@Entity
@Table(name = "itra")
public class Itra {
    @Id
    @SequenceGenerator(name = "itra_id_seq", sequenceName = "itra_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="itra_id_seq")
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "id", nullable = false)
    private User user;

    @Column(name = "point_g")
    private Integer point_g;

    @Column(name = "point_v")
    private Integer point_v;

    @Column(name = "point_xxs")
    private Integer point_xxs;

    @Column(name = "point_xs")
    private Integer point_xs;

    @Column(name = "point_s")
    private Integer point_s;

    @Column(name = "point_m")
    private Integer point_m;

    @Column(name = "point_l")
    private Integer point_l;

    @Column(name = "point_xl")
    private Integer point_xl;

    @Column(name = "point_xxl")
    private Integer point_xxl;

    public Itra() {}

    public Itra(User user, Integer point_g, Integer point_v, Integer point_xxs, Integer point_xs, Integer point_s, Integer point_m, Integer point_l, Integer point_xl, Integer point_xxl) {
        this.user = user;
        this.point_g = point_g;
        this.point_v = point_v;
        this.point_xxs = point_xxs;
        this.point_xs = point_xs;
        this.point_s = point_s;
        this.point_m = point_m;
        this.point_l = point_l;
        this.point_xl = point_xl;
        this.point_xxl = point_xxl;
    }

    public Integer getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public Integer getPoint_g() {
        return point_g;
    }

    public Integer getPoint_v() {
        return point_v;
    }

    public Integer getPoint_xxs() {
        return point_xxs;
    }

    public Integer getPoint_xs() {
        return point_xs;
    }

    public Integer getPoint_s() {
        return point_s;
    }

    public Integer getPoint_m() {
        return point_m;
    }

    public Integer getPoint_l() {
        return point_l;
    }

    public Integer getPoint_xl() {
        return point_xl;
    }

    public Integer getPoint_xxl() {
        return point_xxl;
    }
}

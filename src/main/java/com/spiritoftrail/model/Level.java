package com.spiritoftrail.model;

import jakarta.persistence.*;

@Entity
@Table(name = "level")
public class Level {
    @Id
    @Column(name = "id", nullable = false, updatable = false)
    private String id;

    @Column(name = "name", nullable = false)
    private String name;

    public Level() {}

    public Level(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

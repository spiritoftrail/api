package com.spiritoftrail.model;

import com.spiritoftrail.common.Constants;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "users")
public class User {
    @Id
    @SequenceGenerator(name = "user_id_seq", sequenceName = "user_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_id_seq")
    @Column(name = "id", nullable = false, updatable = false)
    private Integer id;

    @Column(name = "firstname", nullable = false, length = Constants.MODEL_USER_FIRSTNAME_MAXLENGTH)
    private String firstname;

    @Column(name = "lastname", nullable = false, length = Constants.MODEL_USER_LASTNAME_MAXLENGTH)
    private String lastname;

    @Column(name = "email", nullable = false, length = Constants.MODEL_USER_EMAIL_MAXLENGTH)
    private String email;

    @Column(name = "password", nullable = false, length = Constants.MODEL_USER_PASSWORD_MAXLENGTH)
    private String password;

    @Column(name = "birthday", nullable = false)
    private Integer birthday;

    @ManyToOne
    @JoinColumn(name = "country", referencedColumnName = "id", nullable = false)
    private Country country;

    @ManyToOne
    @JoinColumn(name = "gender", referencedColumnName = "id", nullable = false)
    private Gender gender;

    @Column(name = "register", nullable = false)
    private Date register;

    @ManyToOne
    @JoinColumn(name = "level", referencedColumnName = "id", nullable = false)
    private Level level;

    @Column(name = "itra")
    private Integer itra;

    @Column(name = "auto", nullable = false)
    private Boolean auto;

    @Column(name = "active", nullable = false)
    private Boolean active;

    public User() {}

    public User(String firstname, String lastname, String email, String password, Integer birthday, Country country, Gender gender, Date register, Level level, Integer itra, Boolean auto, Boolean active) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.birthday = birthday;
        this.country = country;
        this.gender = gender;
        this.register = register;
        this.level = level;
        this.itra = itra;
        this.auto = auto;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Integer getBirthday() {
        return birthday;
    }

    public Country getCountry() {
        return country;
    }

    public Gender getGender() {
        return gender;
    }

    public Date getRegister() {
        return register;
    }

    public Level getLevel() {
        return level;
    }

    public Integer getItra() {
        return itra;
    }

    public Boolean getAuto() {
        return auto;
    }

    public Boolean getActive() {
        return active;
    }
}

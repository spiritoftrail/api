package com.spiritoftrail.model;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "preference")
public class Preference implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "userid", referencedColumnName = "id", nullable = false, updatable = false)
    private User user;

    @Column(name = "subscription")
    private String subscription;

    @Column(name = "map")
    private String map;

    public Preference() {}

    public Preference(User user, String subscription, String map) {
        this.user = user;
        this.subscription = subscription;
        this.map = map;
    }

    public User getUser() {
        return user;
    }

    public String getSubscription() {
        return subscription;
    }

    public String getMap() {
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Preference that = (Preference) o;
        return Objects.equals(user, that.user) && Objects.equals(subscription, that.subscription) && Objects.equals(map, that.map);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, subscription, map);
    }
}

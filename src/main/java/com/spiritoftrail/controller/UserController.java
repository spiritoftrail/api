package com.spiritoftrail.controller;

import com.spiritoftrail.dto.ItraUserDto;
import com.spiritoftrail.dto.SigninUserDto;
import com.spiritoftrail.dto.SignupUserDto;
import com.spiritoftrail.dto.SignupUserStatusDto;
import com.spiritoftrail.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Create a user")
    @PostMapping(value = "/signup", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SignupUserStatusDto> signupUser(
            @Parameter(description = "User DTO", required = true)
            @RequestBody SignupUserDto user
    ) {
        SignupUserStatusDto signupUserStatusDto = userService.signupUser(user);

        return (signupUserStatusDto != null && signupUserStatusDto.success()) ? new ResponseEntity<>(signupUserStatusDto, HttpStatus.CREATED) : new ResponseEntity<>(signupUserStatusDto, HttpStatus.BAD_REQUEST);
    }

    @Operation(summary = "Login a user")
    @GetMapping(value = "/signin", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SigninUserDto> signinUser(
            @Parameter(description = "Email", required = true)
            @RequestParam("email") String email,
            @Parameter(description = "Password", required = true)
            @RequestParam("password") String password
    ) {
        SigninUserDto signinUserDto = userService.signinUser(email, password);

        return (signinUserDto != null && signinUserDto.success()) ? new ResponseEntity<>(signinUserDto, HttpStatus.OK) : new ResponseEntity<>(signinUserDto, HttpStatus.FORBIDDEN);
    }

    @Operation(summary = "Get all Itra user code")
    @GetMapping(value = "/itra", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ItraUserDto>> itraUser() {
        List<ItraUserDto> itraUserDto = userService.allItraUser();

        return new ResponseEntity<>(itraUserDto, HttpStatus.OK);
    }
}

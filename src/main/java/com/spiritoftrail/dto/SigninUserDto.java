package com.spiritoftrail.dto;

public record SigninUserDto(Boolean success, Integer id, String firstname, String lastname, String level, String error) { }
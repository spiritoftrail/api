package com.spiritoftrail.dto;

import java.util.List;

public record SignupUserStatusDto(Boolean success, List<SignupUserErrorDto> error) { }

package com.spiritoftrail.dto;

public record ItraUserDto(Integer userId, Integer itraCode) { }

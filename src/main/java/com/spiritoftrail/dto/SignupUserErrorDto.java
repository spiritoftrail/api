package com.spiritoftrail.dto;

public record SignupUserErrorDto(String name, String value) { }
package com.spiritoftrail.dto;

public record CountryCodeDto(String code, String name) { }
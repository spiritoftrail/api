package com.spiritoftrail.dto;

public record SignupUserDto(String firstname, String lastname, String email, String password, String repassword, String birthday, String gender, String country) { }
package com.spiritoftrail.repository;

import com.spiritoftrail.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    User findUserByEmail(String email);
    User findByEmailAndPassword(String email, String password);

    List<User> findAll();
}

package com.spiritoftrail.repository;

import com.spiritoftrail.model.Gender;
import org.springframework.data.repository.CrudRepository;

public interface GenderRepository extends CrudRepository<Gender, Integer> {
    Gender findById(String id);
}

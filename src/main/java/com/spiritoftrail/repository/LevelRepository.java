package com.spiritoftrail.repository;

import com.spiritoftrail.model.Level;
import org.springframework.data.repository.CrudRepository;

public interface LevelRepository extends CrudRepository<Level, Integer> {
    Level findByName(String name);
}

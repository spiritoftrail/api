package com.spiritoftrail.repository;

import com.spiritoftrail.model.Country;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CountryRepository extends CrudRepository<Country, Integer> {
    List<Country> findAllByOrderByName();
    Country findById(String id);
}

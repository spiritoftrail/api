package com.spiritoftrail.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PreferenceTest {

    private final User USER = new User();
    private final String SUBSCRIPTION = "SUBSCRIPTION";
    private final String MAP = "MAP";

    Preference preference1, preference2;

    @BeforeEach
    void setUp() {
        preference1 = new Preference();
        preference2 = new Preference(USER, SUBSCRIPTION, MAP);
    }

    @Test
    void getterTest() {
        assertNull(preference1.getUser());
        assertNull(preference1.getSubscription());
        assertNull(preference1.getMap());

        assertEquals(preference2.getUser(), USER);
        assertEquals(preference2.getSubscription(), SUBSCRIPTION);
        assertEquals(preference2.getMap(), MAP);
    }

    @Test
    void equalsTest() {
        assertNotEquals(preference1, preference2);
    }

    @Test
    void testHashCodeForEqualObjects() {
        int hashCode1 = preference1.hashCode();
        int hashCode2 = preference2.hashCode();

        assertThat(hashCode1).isNotEqualTo(hashCode2);
    }
}

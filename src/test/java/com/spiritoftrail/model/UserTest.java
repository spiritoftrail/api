package com.spiritoftrail.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class UserTest {

    private final String FIRSTNAME = "Small is Digital";
    private final String LASTNAME = "Admin";
    private final String EMAIL = "contact@smallisdigital.com";
    private final String PASSWORD = "smallisdigital";
    private final int BIRTHDAY = 1970;
    private final Country COUNTRY = new Country();
    private final Gender GENDER = new Gender();
    private final Date REGISTER = new Date();
    private final Level LEVEL = new Level();
    private final Integer ITRA = 1739104;
    private final boolean AUTO = true;
    private final boolean ACTIVE = true;

    User user1, user2;

    @BeforeEach
    void setUp() {
        user1 = new User();
        user2 = new User(FIRSTNAME, LASTNAME, EMAIL, PASSWORD, BIRTHDAY, COUNTRY, GENDER, REGISTER, LEVEL, ITRA, AUTO, ACTIVE);
    }

    @Test
    void getterTest() {
        assertNull(user1.getId());
        assertNull(user1.getFirstname());
        assertNull(user1.getLastname());
        assertNull(user1.getEmail());
        assertNull(user1.getPassword());
        assertNull(user1.getBirthday());
        assertNull(user1.getCountry());
        assertNull(user1.getGender());
        assertNull(user1.getRegister());
        assertNull(user1.getLevel());
        assertNull(user1.getItra());
        assertNull(user1.getAuto());
        assertNull(user1.getActive());

        assertEquals(user2.getFirstname(), FIRSTNAME);
        assertEquals(user2.getLastname(), LASTNAME);
        assertEquals(user2.getEmail(), EMAIL);
        assertEquals(user2.getPassword(), PASSWORD);
        assertEquals(user2.getBirthday(), BIRTHDAY);
        assertEquals(user2.getCountry(), COUNTRY);
        assertEquals(user2.getGender(), GENDER);
        assertEquals(user2.getRegister(), REGISTER);
        assertEquals(user2.getLevel(), LEVEL);
        assertEquals(user2.getItra(), ITRA);
        assertEquals(user2.getAuto(), AUTO);
        assertEquals(user2.getActive(), ACTIVE);
    }
}

package com.spiritoftrail.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ConnectionTest {

    private final User USER = new User();
    private final Date LAST = new Date();
    private final int NUMBER = 1;

    Connection connection1, connection2;

    @BeforeEach
    void setUp() {
        connection1 = new Connection();
        connection2 = new Connection(USER, LAST, NUMBER);
    }

    @Test
    void getterTest() {
        assertNull(connection1.getUser());
        assertNull(connection1.getLast());
        assertNull(connection1.getNumber());

        assertEquals(connection2.getUser(), USER);
        assertEquals(connection2.getLast(), LAST);
        assertEquals(connection2.getNumber(), NUMBER);
    }

    @Test
    void equalsTest() {
        assertNotEquals(connection1, connection2);
    }

    @Test
    void testHashCodeForEqualObjects() {
        int hashCode1 = connection1.hashCode();
        int hashCode2 = connection2.hashCode();

        assertThat(hashCode1).isNotEqualTo(hashCode2);
    }
}

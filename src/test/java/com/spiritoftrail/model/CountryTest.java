package com.spiritoftrail.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CountryTest {

    private final String ID = "FR";
    private final String NAME = "France";
    private final Integer PHONE = 33;

    Country country1, country2;

    @BeforeEach
    void setUp() {
        country1 = new Country();
        country2 = new Country(ID, NAME, PHONE);
    }

    @Test
    void getterTest() {
        assertNull(country1.getId());
        assertNull(country1.getName());
        assertNull(country1.getPhone());

        assertEquals(country2.getId(), ID);
        assertEquals(country2.getName(), NAME);
        assertEquals(country2.getPhone(), PHONE);
    }
}

package com.spiritoftrail.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GenderTest {

    private final String ID = "M";
    private final String NAME = "Homme";

    Gender gender1, gender2;

    @BeforeEach
    void setUp() {
        gender1 = new Gender();
        gender2 = new Gender(ID, NAME);
    }

    @Test
    void getterTest() {
        assertNull(gender1.getId());
        assertNull(gender1.getName());

        assertEquals(gender2.getId(), ID);
        assertEquals(gender2.getName(), NAME);
    }
}

package com.spiritoftrail.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ItraTest {

    private final User USER = new User();
    private final Integer POINT_G = 500;
    private final Integer POINT_V = 501;
    private final Integer POINT_XXS = 502;
    private final Integer POINT_XS = 503;
    private final Integer POINT_S = 504;
    private final Integer POINT_M = 505;
    private final Integer POINT_L = 506;
    private final Integer POINT_XL = 507;
    private final Integer POINT_XXL = 508;;

    Itra itra1, itra2;

    @BeforeEach
    void setUp() {
        itra1 = new Itra();
        itra2 = new Itra(USER, POINT_G, POINT_V, POINT_XXS, POINT_XS, POINT_S, POINT_M, POINT_L, POINT_XL, POINT_XXL);
    }

    @Test
    void getterTest() {
        assertNull(itra1.getId());
        assertNull(itra1.getUser());
        assertNull(itra1.getPoint_g());
        assertNull(itra1.getPoint_v());
        assertNull(itra1.getPoint_xxs());
        assertNull(itra1.getPoint_xs());
        assertNull(itra1.getPoint_s());
        assertNull(itra1.getPoint_m());
        assertNull(itra1.getPoint_l());
        assertNull(itra1.getPoint_xl());
        assertNull(itra1.getPoint_xxl());

        assertEquals(itra2.getUser(), USER);
        assertEquals(itra2.getPoint_g(), POINT_G);
        assertEquals(itra2.getPoint_v(), POINT_V);
        assertEquals(itra2.getPoint_xxs(), POINT_XXS);
        assertEquals(itra2.getPoint_xs(), POINT_XS);
        assertEquals(itra2.getPoint_s(), POINT_S);
        assertEquals(itra2.getPoint_m(), POINT_M);
        assertEquals(itra2.getPoint_l(), POINT_L);
        assertEquals(itra2.getPoint_xl(), POINT_XL);
        assertEquals(itra2.getPoint_xxl(), POINT_XXL);
    }
}

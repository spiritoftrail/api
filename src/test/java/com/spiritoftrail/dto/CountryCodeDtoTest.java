package com.spiritoftrail.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CountryCodeDtoTest {

    private final String CODE = "FR";
    private final String NAME = "France";

    CountryCodeDto countryCodeDto;

    @BeforeEach
    void setUp() {
        countryCodeDto = new CountryCodeDto(CODE, NAME);
    }

    @Test
    void recordTest() {
        assertEquals(countryCodeDto.code(), CODE);
        assertEquals(countryCodeDto.name(), NAME);
    }
}

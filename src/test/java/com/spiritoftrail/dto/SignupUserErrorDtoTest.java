package com.spiritoftrail.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SignupUserErrorDtoTest {

    private final String NAME = "NAME";
    private final String VALUE = "VALUE";

    SignupUserErrorDto signupUserErrorDtoTest;

    @BeforeEach
    void setUp() {
        signupUserErrorDtoTest = new SignupUserErrorDto(NAME, VALUE);
    }

    @Test
    void recordTest() {
        assertEquals(signupUserErrorDtoTest.name(), NAME);
        assertEquals(signupUserErrorDtoTest.value(), VALUE);
    }
}

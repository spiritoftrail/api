package com.spiritoftrail.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SigninUserDtoTest {

    private final boolean SUCCESS = true;
    private final int ID = 1;
    private final String FIRSTNAME = "Small is Digital";
    private final String LASTNAME = "Admin";
    private final String LEVEL = "V";

    private final String ERROR = "No content";

    SigninUserDto signinUserDto;

    @BeforeEach
    void setUp() {
        signinUserDto = new SigninUserDto(SUCCESS, ID, FIRSTNAME, LASTNAME, LEVEL, ERROR);
    }

    @Test
    void recordTest() {
        assertEquals(signinUserDto.success(), SUCCESS);
        assertEquals(signinUserDto.id(), ID);
        assertEquals(signinUserDto.firstname(), FIRSTNAME);
        assertEquals(signinUserDto.lastname(), LASTNAME);
        assertEquals(signinUserDto.level(), LEVEL);
        assertEquals(signinUserDto.error(), ERROR);
    }
}

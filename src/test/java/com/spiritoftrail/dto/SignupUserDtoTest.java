package com.spiritoftrail.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SignupUserDtoTest {

    private final String FIRSTNAME = "Spirit of Trail";
    private final String LASTNAME = "Admin";
    private final String EMAIL = "contact@spiritoftrail.com";
    private final String PASSWORD = "spiritoftrail";
    private final String REPASSWORD = "spiritoftrail";
    private final String BIRTHDAY = "2000";
    private final String GENDER = "M";
    private final String COUNTRY = "FR";

    SignupUserDto signupUserDto;

    @BeforeEach
    void setUp() {
        signupUserDto = new SignupUserDto(FIRSTNAME, LASTNAME, EMAIL, PASSWORD, REPASSWORD, BIRTHDAY, GENDER, COUNTRY);
    }

    @Test
    void recordTest() {
        assertEquals(signupUserDto.firstname(), FIRSTNAME);
        assertEquals(signupUserDto.lastname(), LASTNAME);
        assertEquals(signupUserDto.email(), EMAIL);
        assertEquals(signupUserDto.password(), PASSWORD);
        assertEquals(signupUserDto.repassword(), REPASSWORD);
        assertEquals(signupUserDto.birthday(), BIRTHDAY);
        assertEquals(signupUserDto.gender(), GENDER);
        assertEquals(signupUserDto.country(), COUNTRY);
    }
}

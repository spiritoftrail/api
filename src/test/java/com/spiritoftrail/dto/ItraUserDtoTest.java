package com.spiritoftrail.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ItraUserDtoTest {

    private final Integer USERID = 1;
    private final Integer ITRACODE = 1739104;

    ItraUserDto itraUserDto;

    @BeforeEach
    void setUp() {
        itraUserDto = new ItraUserDto(USERID, ITRACODE);
    }

    @Test
    void recordTest() {
        assertEquals(itraUserDto.userId(), USERID);
        assertEquals(itraUserDto.itraCode(), ITRACODE);
    }
}

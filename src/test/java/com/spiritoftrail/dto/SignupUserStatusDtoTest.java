package com.spiritoftrail.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SignupUserStatusDtoTest {

    private final boolean SUCCESS = false;
    private final List<SignupUserErrorDto> ERROR = new ArrayList<>();

    SignupUserStatusDto signupUserStatusDto;

    @BeforeEach
    void setUp() {
        signupUserStatusDto = new SignupUserStatusDto(SUCCESS, ERROR);
    }

    @Test
    void recordTest() {
        assertEquals(signupUserStatusDto.success(), SUCCESS);
        assertEquals(signupUserStatusDto.error(), ERROR);
    }
}

